<?php
/**
 * Implementation of hook_elements().
 *
 */
function fullpage_bg_elements()
{
    $types = array();
    $types['fullpage_bg_upload_form'] = array(
        '#input' => TRUE,
        '#process' => array('fullpage_bg_upload_form_process'),
        '#element_validate' => array('fullpage_bg_upload_form_validate'),
    );
    return $types;
}

/**
 * Implementation of hook_theme(). This defines the default theming function
 * for the fullpage_bg_upload_form element.
 *
 */
function fullpage_bg_theme()
{
    return array(
        'fullpage_bg_upload_form' => array(
            'arguments' => array('element' => NULL),
        ),
    );
}

/**
 * Theming function for fullpage_bg_upload_form.
 *
 */
function theme_fullpage_bg_upload_form($element)
{
    return theme('form_element', $element, '<div class="fullpage_bg-settings-row clear-block">' . $element['#children'] . '</div>');
}

/**
 * Implementation of hook_form_process(). Handles the fullpage_bg_upload_form
 * element processing and building.
 *
 */
function fullpage_bg_upload_form_process($element, $form_state)
{
    $element['#tree'] = TRUE;

    if ($element['#value']['default']) {
        $picture = theme('imagecache', 'fullpage_bg_thumb', $element['#value']['default'], t('Image preview'), t('Image preview'), NULL);
        $element['current_picture'] = array(
            '#value' => $picture
        );
        $element['fullpage_bg_path'] = array(
            '#type' => 'textarea',
            '#title' => t('Show background image on specific pages.'),
            '#cols' => 40,
            '#rows' => 5,
            '#default_value' => $element['#value']['fullpage_bg_path'],
            '#description' => t('Show only on listed pages.'),
            '#prefix' => '<div class="fullpage_bg-text">',
            '#suffix' => '</div>',
        );
        $element['fullpage_bg_overly_image'] = array(
            '#type' => 'checkbox',
            '#title' => t('Use overlay effect'),
            '#default_value' => $element['#value']['fullpage_bg_overly_image'],
        );
        $element['fullpage_bg_page_delete'] = array(
            '#type' => 'checkbox',
            '#title' => t('Delete Page'),
            '#default_value' => $element['#value']['fullpage_bg_image_delete'],
        );

    } else {
        $element['fullpage_bg_path'] = array(
            '#type' => 'textarea',
            '#title' => t('Show background image on specific pages.'),
            '#cols' => 40,
            '#rows' => 5,
            '#description' => t('Show only on listed pages.'),
            '#prefix' => '<div class="fullpage_bg-text">',
            '#suffix' => '</div>',
        );
        $element['fullpage_bg_file_upload'] = array(
            '#type' => 'file',
            '#title' => 'File Upload',
            '#tree' => TRUE,
            '#prefix' => '<div class="fullpage_bg-file-upload">',
            '#suffix' => '</div>'
        );
    }
    return $element;
}

/**
 * Validation of the fullpage_bg_upload_form element. It have to return the
 * $form.
 *
 */
function fullpage_bg_upload_form_validate($element, &$form_state)
{
    return $form_state;
}

/**
 * Implementation of hook_imagecache_default_presets().
 */
function fullpage_bg_imagecache_default_presets()
{
    $items = array(
        'fullpage_bg_thumb' => array(
            'presetname' => 'fullpage_bg_thumb',
            'actions' => array(
                '0' => array(
                    'weight' => '0',
                    'module' => 'imagecache',
                    'action' => 'imagecache_scale_and_crop',
                    'data' => array(
                        'width' => '100',
                        'height' => '100',
                    ),
                ),
            ),
        ),
    );
    return $items;
}