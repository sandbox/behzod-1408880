<?php

/**
 * Boshida forma nechta elementdan tashkil topishi kerakligi ko'rsatilgan.
 */

DEFINE('FULLPAGE_BG_FORM_ELEMENTS', 3);

function fullpage_bg_settings_page($op = NULL)
{
    $output = drupal_get_form('fullpage_bg_file_upload_form');
    return $output;
}

/**
 * Bu funksiya forma yaratish vazifasini bajaradi.
 */

function fullpage_bg_file_upload_form()
{
    $form = array();
    $fullpage_bg_images = simpleUnserialize(variable_get('fullpage_bg_images', null));
    $form['#attributes'] = array('enctype' => 'multipart/form-data');
    for ($i = 0; $i < (count($fullpage_bg_images) + FULLPAGE_BG_FORM_ELEMENTS); $i++) {
        $value = isset($fullpage_bg_images[$i]) ? $fullpage_bg_images[$i] : null;
        $form['fullpage_bg_form' . $i] = array(
            '#type' => 'fullpage_bg_upload_form',
            '#value' => $value,
        );
    }
    $form = system_settings_form($form);

    $form['#validate'][] = 'fullpage_bg_images_validate';
    $form['#submit'] = array('fullpage_bg_image_submit');
    return $form;
}

/**
 * Bu funksiya forma jo'natilyatganda formani tekshirish jarayonini amalga oshiradi.
 */

function fullpage_bg_images_validate($form, &$form_state)
{
    if (!empty($_FILES)) {
        foreach ($_FILES['files']['name'] as $file_field => $value) {
            if ($value == '') {
                continue;
            }
            if ($file = file_save_upload($file_field)) {
                $destination = (file_directory_path()) . "/" . "fullpage_bg";
                if (!file_exists($destination)) {
                    mkdir($destination, 0777);
                }
                if (file_move($file, $destination, FILE_EXISTS_REPLACE)) {
                    $form_state['values'][$file_field]['default'] = $file->filepath;
                }
                else {
                    form_set_error("");
                }
            }
        }
    }

    foreach ($form_state['values'] as $k => $v) {
        if ($v['default'] and $v['fullpage_bg_path'] == null) {
            form_set_error($k, t('Path is required.'));
        }
    }
}

/**
 * Bu funksiya form ma validate qilib bo'lingandan kiyin malumotlarni kerakli joylarga saqalash vazifasini bajaradi.
 */

function fullpage_bg_image_submit($form, &$form_state)
{
    
    unset($form_state['values']['submit'], $form_state['values']['reset'], $form_state['values']['form_id'], $form_state['values']['op'], $form_state['values']['form_token'], $form_state['values']['form_build_id']);
    $images_config = unserialize(variable_get('fullpage_bg_images', ""));
    foreach ($form_state['values'] as $value) {
        if (isset($value['default'])) {
            if (!isSaved($value['default'], $images_config) and $value['fullpage_bg_page_delete'] != 1 and isset($value['default'])) {
                $images_config[] = $value;
            } elseif (isSaved($value['default'], $images_config)) {
                changeItem($value, $images_config);
            }
            if ($value['fullpage_bg_page_delete'] == 1 and isSaved($value['default'], $images_config)) {
                removeItem($value['default'], $images_config);
            }
        }
    }
    variable_set('fullpage_bg_images', serialize($images_config));
    drupal_set_message(t("The configuration was saved successfully"));
}

/**
 * Bu funksiya boolean qaytaradi yani true yoki false.
 * Bu funksiya orqali malumotlar bazaga saqlandimi yoki saqlanmadimi shu vazifani bajaradi
 */

function isSaved($default, $array)
{
    if (!empty($array)) {
        foreach ($array as $item) {
            if ($item['default'] == $default and strlen($default) > 0)
                return true;
        }
    }
    return false;
}

/**
 * Bu funksiya ko'rsatilgan fileni o'chirish vazifasini bajaradi.
 */

function removeItem($default, &$array)
{
    foreach ($array as $k => $v) {
        if ($array[$k]['default'] == $default) {
            unset($array[$k]);
            delete_bg_file($default);
        }
    }
    return;
}

/**
 * Bu funksiya malumot birinchi marta kiritilgandan kiyin yana o'sha malumotga o'zgartirish kiritilgada foydalaniladi.
 * Yani o'zgartirish kiritilganimi yo yo'qligini aniqlaydi va o'sha oldongi malumot bilan ohitgi malumotni almashtiradi.
 */

function changeItem($rowArray, &$array)
{
    foreach ($array as $k => $v) {
        if ($array[$k]['default'] == $rowArray['default']) {
            $array[$k] = $rowArray;
        }
    }
    return;
}

/**
 * Bu funksiya upload qilingan imageni path ini olish vazifasini bajaradi
 */

function getImagePath()
{
    return file_directory_path() . "/fullpage_bg/";
}

/**
 * Bu funksiya ko'rsatilgan image ni o'chirish vasizfasini bajaradi
 */

function delete_bg_file($path)
{
    file_delete(getImagePath() . end(explode("/", $path)));
    imagecache_image_flush($path);
}

/**
 * Bu funksiya serialize qilingan malumotlarni o'z holiga qaytarish vazifasini bajaradi
 */

function simpleUnserialize($data)
{
    if ($data)
        return unserialize($data);
    return array();
}